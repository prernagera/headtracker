/**
 * @author: Vikas Dhiman
 */
#include <headtracker/maskto3d.h>

#include <headtracker/objectDetection.h>
#include <headtracker/horn.h>

#include <opencv2/opencv.hpp>
#include <Eigen/Dense>

#include <vector>

using std::vector;
using cv::Mat;
using cv::Rect;
using Eigen::Matrix3f;
using Eigen::MatrixXf;
using namespace cv;

// Using global variables to maintain previous image state for tracking (too bad)
// FIXME: convert to a class
cv::Mat prevGray;
cv::Mat prevMask;
cv::Mat prevDepth;

/******************************************************************************/
Matrix3f intrinsic_matrix(const Mat &image) {
    double focal_length = 613.08;
  Matrix3f K = Matrix3f::Identity();
  K(0, 0) = focal_length;
  K(1, 1) = focal_length;
  K(0, 2) = float(image.cols)/2.;
  K(1, 2) = float(image.rows)/2.;
  return K;
}

/******************************************************************************/
// Not used . Logic is replicated in extractFeatures
vector<vector<double> > maskto3D(const Mat &depth, Rect face_rect) 
{
  Matrix3f K = intrinsic_matrix(depth);
  int x = face_rect.x;
  int y = face_rect.y;
  int row = y;
  int col = x;
  int delta_row = face_rect.height;
  int delta_col = face_rect.width;
  MatrixXf pts2d(2, delta_row * delta_col); 
  vector<double> depth_pts;
  for (int r = row; r < row + delta_row; r++) {
      for (int c = col; c < col + delta_col; c++) {
          pts2d(0, r*delta_col + c) = r;
          pts2d(1, r*delta_col + c) = c;
          depth_pts.push_back(depth.at<double>(r, c));
      }
  }
  return reconstruct_pts(K, pts2d, depth_pts);
}

/******************************************************************************/
Eigen::Matrix4f
extractFeatures(const Mat &image, Rect rect, const Mat &depth, Mat &vis) {
    vector<Point2f> points, prevPoints;
    cv::Mat gray, mask;
    int maxCorners = 100;
    double qualityLevel = 0.01;
    double minDistance  = 5;
    Size winSize(31, 31);
    TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 20, 0.03);
    
    // Feature point extraction only works on grayscale images
    cv::cvtColor(image, gray, CV_BGR2GRAY);

    // Create a stencil to extract the detected face region.
    mask = Mat(image.size(), CV_8UC1);
    mask.setTo(Scalar::all(0));
    Mat maskroi = mask(rect);
    maskroi.setTo(Scalar::all(255));

    /// Initially prev* images can be empty
    if (prevGray.empty())
      gray.copyTo(prevGray);
    if (prevDepth.empty())
      depth.copyTo(prevDepth);
    if (prevMask.empty())
      mask.copyTo(prevMask);

    vector<uchar> status;
    vector<float> err;

    // Extract LKT features from prevGray image 
    goodFeaturesToTrack(prevGray, prevPoints, maxCorners, qualityLevel,
        minDistance, prevMask);
    // Find corresponding LKT features in the new gray image
    calcOpticalFlowPyrLK(prevGray, gray, prevPoints, points, status, err,
        winSize, 3, termcrit, 0, 0.001);

    // Draw one some of those points on the visualizer
    for (int i = 0; i < prevPoints.size(); i++)
      cv::line(vis, prevPoints[i], points[i], Scalar(0, 255, 0), 1);

    // Converts points from prevGray to 3D ///////////////////////////
    Matrix3f K = intrinsic_matrix(image);
    MatrixXf prev_pts2d(2, prevPoints.size());
    vector<double> prev_depth_pts;
    for (int i = 0; i < prevPoints.size(); i++) {
        cv::Point2f pt = prevPoints[i];
        prev_pts2d(0, i) = pt.x;
        prev_pts2d(1, i) = pt.y;
        prev_depth_pts.push_back(prevDepth.at<float>(pt.y, pt.x));
    }
    vector<vector<double> > prev_p3d = 
      reconstruct_pts(K, prev_pts2d, prev_depth_pts);
    // end of Converts points from prevGray to 3D ////////////////////

    // Converts points from gray to 3D ///////////////////////////
    MatrixXf pts2d(2, points.size());
    vector<double> depth_pts;
    for (int i = 0; i < points.size(); i++) {
        cv::Point2f pt = points[i];
        pts2d(0, i) = pt.x;
        pts2d(1, i) = pt.y;
        depth_pts.push_back(depth.at<float>(pt.y, pt.x));
    }
    vector<vector<double> > p3d = 
      reconstruct_pts(K, pts2d, depth_pts);
    // end of Converts points from gray to 3D ////////////////////

    // Filter out Nan points ///////////////////////////////////////
    vector<vector<double> > p3d_not_nan;
    vector<vector<double> > prev_p3d_not_nan;
    for (int i = 0; i < p3d.size(); i++) {
      if (std::isnan(p3d[i][0]) ||
          std::isnan(p3d[i][1]) ||
          std::isnan(p3d[i][2]) ||
          std::isnan(prev_p3d[i][0]) ||
          std::isnan(prev_p3d[i][1]) ||
          std::isnan(prev_p3d[i][2]))
        continue;
      p3d_not_nan.push_back(p3d[i]);
      prev_p3d_not_nan.push_back(prev_p3d[i]);
    }
    // end of Filter out Nan points ////////////////////////////////

    // Find transformation b/w 3D points
    Eigen::Matrix4f T = horn(p3d_not_nan, prev_p3d_not_nan);

    // Copy the state for next iteration
    gray.copyTo(prevGray);
    mask.copyTo(prevMask);
    depth.copyTo(prevDepth);

    return T;
}

/******************************************************************************/
vector<vector<double> > reconstruct_pts(Matrix3f K, MatrixXf pts2d,
    vector<double> depth_pts) {

  // Convert euclidean to homogeneous
  MatrixXf pts3d(3, pts2d.cols()); 
  for (int c = 0; c < pts2d.cols(); c++) {
      pts3d(0, c) = pts2d(0, c);
      pts3d(1, c) = pts2d(1, c);
      pts3d(2, c) = 1; 
  }

  MatrixXf pointCloud = K.inverse() * pts3d;

  // scale the 3D point to appropriate depth
  vector<vector<double> > vect_pts;//(pts3d.cols(), vector<double>(3,0)); 
  for (int c = 0; c < pointCloud.cols(); c++) {
      float scale = depth_pts[c] / pointCloud(2, c);
      vector<double> this_pt(3, 0);
      this_pt[0] = pointCloud(0, c) * scale;
      this_pt[1] = pointCloud(1, c) * scale;
      this_pt[2] = pointCloud(2, c) * scale;
      vect_pts.push_back(this_pt);
  }
  return vect_pts;
}

