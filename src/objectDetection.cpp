/**
 * @file objectDetection.cpp
 * @author Suren Kumar (Originally by A. Huaman) ( based in the classic facedetect.cpp in samples/c )
 * @brief A simplified version of facedetect.cpp, show how to load a cascade classifier and how to find objects (Face + eyes) in a video stream acquired using ASUS Xtion

// To run this file use for just detection of faces
bin/face_det ../data/haarcascade_frontalface_alt.xml 
// To run this file with faces and eyes with appropriate lines uncommented from code
bin/face_det ../data/haarcascade_frontalface_alt.xml ../data/haarcascade_eye_tree_eyeglasses.xml
once you are inside the build folder

// faces_roi stores the face mask from depth image and
faces_req stores all the faces detected in cv Rect format
*/
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
//#include "opencv2/core/utility.hpp"

#include "opencv2/highgui/highgui_c.h"

#include <iostream>
#include <stdio.h>

#include <headtracker/io.h>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

#include <headtracker/objectDetection.h>

using namespace std;
using namespace cv;

/** Global variables */
//-- Note, either copy these two files from opencv/data/haarscascades to your current folder, or change these locations
//string face_cascade_name = "/home/robot/suren/headtracker/haarcascade_frontalface_alt.xml";
//string eyes_cascade_name = "/home/robot/suren/headtracker/haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
//CascadeClassifier eyes_cascade;
string window_name = "Where you looking at";
string window_name1 = "Where you looking at1";

// Storing initialization of face bounding box
bool initialized = false;
Rect lastbox;
RNG rng(12345);

int detectAndTrack(string face_cascade_name, Mat *frame_ptr, std::vector<Rect> *faces_req_ptr) {

    detectAndDisplayFull( face_cascade_name, frame_ptr, faces_req_ptr);
    Mat frame = *frame_ptr;
    std::vector<Rect> faces_req = *faces_req_ptr;

    //cout<<faces_req.size()<<endl;
    int maxind = -1;
    if (faces_req.size()>0) { 
	maxind = 0;

	// To initialize, we store a boolean array
	if (initialized==false) {
	    initialized = true;
	    // Storing the first face image
	    lastbox = faces_req[0];
	}
	// there was a box in last frame and we need to pick a box 
	else {
	    // Checking the number of detections in current frame
	    if (faces_req.size()>1) { // Pick the relevant face
		// Checking pascal intersection ratio for all the detections
		//std::vector<float> pratio;
		float pratio_max = 0.0;
		float pratio = 0.0;
		for (int numface = 0; numface < faces_req.size();numface++){
		    Rect pratio_rect = faces_req[numface] & lastbox;
            float divisor = (faces_req[numface].width*faces_req[numface].height
			  + lastbox.width*lastbox.height
			  -2*pratio_rect.width*pratio_rect.height
		       );
            if (divisor != 0) {
                pratio = (pratio_rect.width*pratio_rect.height) / divisor;
            } else {
                continue;
            }
		    if (pratio_max<pratio)
		      {
			maxind = numface;
		      }
		}

		cout<<faces_req[maxind].x<<" "<<faces_req[maxind].y <<
		  " "<<faces_req[maxind].width<<" "<<faces_req[maxind].height<<endl;
		Point center( faces_req[maxind].x + faces_req[maxind].width/2,
		    faces_req[maxind].y + faces_req[maxind].height/2 );
		ellipse(frame, center, Size( faces_req[maxind].width/2,faces_req       
		      [maxind].height/2),0, 0, 360, Scalar( 255, 0, 255 ), 2, 8, 0 );
		putText(frame, "Head: ", cvPoint(faces_req[maxind].x,faces_req
		      [maxind].y),FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(255,255,0), 1,CV_AA);
		// Giving the ROI mask from depth image for the required face
		//faces_roi.push_back(depth(faces_req[maxind]));
	    }

	    else{// There is really just one face in the image
		maxind = 0;
		cout<<faces_req[0].x<<" "<<faces_req[0].y <<
		  " "<<faces_req[0].width<<" "<<faces_req[0].height<<endl;
		Point center( faces_req[0].x + faces_req   [0].width/2,
		    faces_req[0].y + faces_req[0].height/2 );
		ellipse(frame, center, Size( faces_req[0].width/2,faces_req       
		      [0].height/2),0, 0, 360, Scalar( 255, 0, 255 ), 2, 8, 0 );
		putText(frame, "Head: ", cvPoint(faces_req[0].x,faces_req[0].y), 
		    FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(255,255,0), 1, CV_AA);
		// Giving the ROI mask from depth image for the required face
		//faces_roi.push_back(depth(faces_req[0]));
	    }
	}
    }
    else{
	maxind = -1;
	// Person exited from the scene or not initialized
	if (initialized==true) {
	    initialized=false;
	}
    }
    return maxind;
}
void detectAndDisplayFull(string face_cascade_name, Mat *frame, std::vector<Rect> *faces_req) {
  if( !face_cascade.load( face_cascade_name ) ) {
      printf("--(!)Error loading\n");
  };
  detectAndDisplay(frame, faces_req);
}
/**
 * @function detectAndDisplay
 */
void detectAndDisplay( Mat *frame,std::vector<Rect> *faces_req)
{
  std::vector<Rect> faces;
  // Filtering for the right types of faces --removing outliers using area
  //std::vector<Rect> faces_req = *faces_ptr;
  // Giving out RGB masks for each face
  //std::vector<Mat> faces_roi = *faces_roi_ptr;
  Mat frame_gray;

  cvtColor( *frame, frame_gray, COLOR_BGR2GRAY );
  equalizeHist( frame_gray, frame_gray );
  //-- Detect faces
  face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2,
      0|CASCADE_SCALE_IMAGE, Size(30, 30) );

  // Storing the faces we need from all the faces we are getting
  for( size_t i = 0; i < faces.size(); i++ ) {
      Point center( faces[i].x + faces[i].width/2,
	  faces[i].y + faces[i].height/2 );
       
       Mat frame_ref = *frame;
      // Filtering for faces by the area we expect
      if (
	  ((faces[i].width/2)*(faces[i].height/2)>=(frame_ref.rows*frame_ref.cols)/1000)
	  &&
	  ((faces[i].width/2)*(faces[i].height/2)<=(frame_ref.rows*frame_ref.cols)/4)
	 ) {
	  //ellipse( *frame, center, Size( faces[i].width/2, faces[i].height/2),
	   //   0, 0, 360, 		Scalar( 255, 0, 255 ), 2, 8, 0 );
	  // Storing only the faces that pass this criteria
	  faces_req->push_back(faces[i]);
      }    

      /*std::vector<Rect> eyes;

      //-- In each face, detect eyes
      eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CASCADE_SCALE_IMAGE, Size(30, 30) );

      for( size_t j = 0; j < eyes.size(); j++ )
      {
      Point eye_center( faces[i].x + eyes[j].x + eyes[j].width/2, faces[i].y + eyes[j].y + eyes[j].height/2 );
      int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
      circle( frame, eye_center, radius, Scalar( 255, 0, 0 ), 3, 8, 0 );
      }*/
    }
  //-- Show what you got
  //cout<<"Look Ma"<<faces_req->size()<<endl;
  /*
  if (faces_req->size()>0) {
      std::vector<Rect> rects = *faces_req;
      putText(*frame, "Head: ", cvPoint(rects[0].x,rects[0].y), 
	  FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(255,255,0), 1, CV_AA);
  }*/
  
}
