/**
 * @file:horn.cpp
 * @author: Prerna Gera
 */
#include <Eigen/Eigen>
#include<iostream>
#include<Eigen/Dense>
#include <vector>
using namespace Eigen;
using namespace std;

/******************************************************************************/
vector<double> mean(vector<vector<double> > v)
{
  int n= v.size();
  vector<double> m(3,0);
  for(size_t i=0; i<n; ++i){
      m[0] += v[i][0];
      m[1] += v[i][1];
      m[2] += v[i][2];
  }

  m[0]=m[0]/n;
  m[1]=m[1]/n;
  m[2]=m[2]/n;
  return m;
}

/******************************************************************************/
MatrixXf covariance(vector<vector<double> > a, vector<vector<double> > b)
{
  int n= a.size();
  int m= b.size();

  MatrixXf va(n,3);
  MatrixXf vb(m,3);
  MatrixXf C(3,3);

  vector<double> Ma=mean(a);
  vector<double> Mb=mean(b);
  for(size_t i=0; i<n; ++i){
      va(i,0)=a[i][0]-Ma[0];
      va(i,1)=a[i][1]-Ma[1];
      va(i,2)=a[i][2]-Ma[2]; 	
  }

  //cout<<"VA \n"<< Ma[0];
  for(size_t i=0; i<m; ++i){
      vb(i,0)=b[i][0]-Mb[0];
      vb(i,1)=b[i][1]-Mb[1];
      vb(i,2)=b[i][2]-Mb[2]; 	
  }

  C=vb.transpose()*va;
  //cout<<C;
  return C;
}


/******************************************************************************/
Eigen::Matrix4f horn(vector<vector<double> > x,vector<vector<double> >y )
{
  MatrixXf C(3,3);
  C= covariance(x,y);
  JacobiSVD<MatrixXf> svdOfA(C,ComputeFullU|ComputeFullV);      
  const Eigen::MatrixXf U = svdOfA.matrixU();
  const Eigen::MatrixXf V = svdOfA.matrixV();
  const Eigen::VectorXf S = svdOfA.singularValues();
  std::cout<<"Matrix U:\n" <<U<<std::endl;
  std::cout<<"Matrix V:\n" <<V<<std::endl;
  std::cout<<"Matrix S:\n" <<S<<std::endl;      
  Matrix3f diag = Matrix3f::Identity();
  diag(2,2)=(U*V.transpose()).determinant();
  //cout<<"Vt"<<V.transpose();
  //cout<<(U*V.transpose())	;
  Matrix3f R;
  R=U*diag*V.transpose();

  Vector3f xm;
  Vector3f ym;
  Vector3f d;
  Vector3f Rx;
  vector<double> xmean= mean(x);
  xm(0)=xmean[0];
  xm(1)=xmean[1];
  xm(2)=xmean[2];	
  vector<double> ymean= mean(y);
  ym(0)=ymean[0];
  ym(1)=ymean[1];
  ym(2)=ymean[2];	
  Rx=R*xm;	
  d(0)=ym(0)-Rx(0);
  d(1)=ym(1)-Rx(1);
  d(2)=ym(2)-Rx(2);	

  Eigen::Matrix4f T = Eigen::Matrix4f::Identity();
  for (int r = 0; r < 3; r++) {
      for (int c = 0; c < 3; c++) {
          T(r, c) = R(r, c);
      }
  }
  for (int r = 0; r < 3; r++) {
      T(r, 3) = d(r);
  }
  cout<<"R:"<<R;
  return T;
  //R*mean(x)
}

