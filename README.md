# Making
cd build/ && cmake .. && make; cd -

# Usage:
Plugin Asus xtion live pro to your computer. Face the asus such that it can
look at your face.

    cd data/
    ../build/bin/face_det SingleHead.vtk haarcascade_frontalface_alt.xml
    
# Tasks
PG = Prerna Gera
SK = Suren Kumar
VD = Vikas Dhiman

* [X] VD: Prepare repository
* [X] VD: Prepare head 3D model
* [X] SK: Check installation of required libraries
	* [X] Tried deva raman's code. Couldn't make it run.
* [X] PG: affine3d_from_points
	* [X] run SVD
	* [X] Horn's method
* [X] VD: Input RGB image and Depth image
	* [X] basic version done
	* [X] add mutex to make it thread safe
* [X] SK: face detection and landmark detection
	* [X] face detection with opencv
	* [X] interface with prerna's code
* [X] VD: Visualization by rotation and translation.

# Sources and credits
* Human face 3D mesh: http://www.sharecg.com/v/19082/related/5/3D-Model/Human-Head-Pack

# Libraries used
* PCL : point cloud library 
* Eigen : Linear algebra library
* Face detection and feature extraction: Deva raman's code
