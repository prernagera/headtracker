/**
 * @author: Suren Kumar
 */
#ifndef HEADTRACKER_OBJECTDETECTION_H
#define HEADTRACKER_OBJECTDETECTION_H
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui_c.h"

using namespace cv;

/** Detect and display faces. */
void detectAndDisplay( cv::Mat *frame,std::vector<cv::Rect> *faces_ptr);

/** Detect and display faces. In addition to detectAndDisplay(),  also
 * initializes the trained classifier from file @param face_cascade_name
 */
void detectAndDisplayFull(std::string face_cascade_name, cv::Mat *frame,std::vector<cv::Rect> *faces_ptr);

/**
 * Detect and filter out the faces that are not within a certain distance of
 * previous face
 */
int detectAndTrack(std::string face_cascade_name, cv::Mat *frame_ptr, std::vector<cv::Rect> *faces_req_ptr);
#endif
