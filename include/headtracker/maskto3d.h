/**
 * @file: maskto3d.h
 * @author: Vikas Dhiman
 */
#ifndef HEADTRACKER_MASKTO3D_H
#define HEADTRACKER_MASKTO3D_H
#include <vector>
#include <opencv2/opencv.hpp>
#include <Eigen/Core>

/**
 * Convert detected face mask and depth image to 3D points
 * @param depth: depth image
 * @param face_rect: ROI of detected facse
 */
std::vector<std::vector<double> > maskto3D(const cv::Mat &depth, cv::Rect face_rect);

/** 
 * Reconstruct points from depth image 
 * @param K : intrinsic camera matrix
 * @param pts2d : list of 2D interest points
 * @param depths: corresponding depths of the pts3d
 */
std::vector<std::vector<double> > reconstruct_pts(Eigen::Matrix3f K, Eigen::MatrixXf pts2d,
    std::vector<double> depths);

/**
 * Find good features to track in the detected face region and track it
 * between two frames using global variables, and compute rigid
 * transformations between the set of points.
 *
 * @param image: new RGB image
 * @param rect: detected face
 * @param depth: corresponding depth image
 * @param vis: Visualizing image
 * @return : Rigid transformation matrix between two frames.
 */
Eigen::Matrix4f
extractFeatures(const cv::Mat &image, cv::Rect rect, const cv::Mat &depth, cv::Mat &vis);

/// Intrinsic camera matrix using image resolution.
Eigen::Matrix3f intrinsic_matrix(const cv::Mat &image);
#endif
