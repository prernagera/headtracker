/**
 * @author: Vikas Dhiman
 */
#include <stdio.h>

#include <headtracker/io.h>
using std::cout;
using std::endl;

int main(int argc, char** argv) {
   RGBD_IO v;
   boost::function<void ()> workerfun = v.run();
   boost::thread workerthread(workerfun);
   while (!v.is_ready()) {
       sleep(1);
   }
   //cout << "Found ready" << endl;
   for (int i = 0; i < 1000; i++) {
       cv::Mat image = v.initImage();
       //cout << image.type();

       cv::Mat depth = v.initDepth();
       v.getImageAndDepth(&image, &depth);
       //cout<< "hi" << endl;

       cv::cvtColor(image, image, CV_RGB2BGR);
       cv::imshow("r", image);
       cv::imshow("d", depth);
       cv::waitKey(33);
   }
   v.shutdown();
   workerthread.join();
   return 0;
}
