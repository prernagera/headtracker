/**
 * @file: test_face_detection.cpp
 * @author: Suren Kumar, Vikas Dhiman
 */
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataReader.h>
#include <vtkActor.h>
#include <vtkCommand.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTransform.h>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
//#include "opencv2/core/utility.hpp"

#include "opencv2/highgui/highgui_c.h"

#include <iostream>
#include <stdio.h>

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

#include <headtracker/io.h>
#include <headtracker/objectDetection.h>
#include <headtracker/maskto3d.h>

void rotateAndTranslate(vtkActor *actor, Eigen::Matrix4f T) {
    double elements[16];
    for (int r =0; r < 4; r ++ ) {
      for (int c=0; c < 4; c++ ) {
          if (std::isnan(T(r, c))) {
              return;
          }
          elements[r*4 + c] = T(r, c);
      }
    }
    vtkSmartPointer<vtkTransform> transf = 
      vtkSmartPointer<vtkTransform>::New();
    transf->SetMatrix(elements);
    cout << "vtkTransform:" << transf << endl;
    double* pos = transf->GetPosition();
    cout << "Position:" << pos[0] << " " << pos[1] << " " << pos[2] << endl;
    double* curpos = actor->GetPosition();
    curpos[0] += pos[0];
    curpos[1] += pos[1];
    curpos[2] += pos[2];
    actor->SetPosition(curpos);
    double* orientation = transf->GetOrientation();
    // double orientation[3];
    // orientation[1] = 10;
    // orientation[2] = 0;
    // orientation[0] = 0;
    // double* wxyz = transf->GetOrientationWXYZ();

    // cout << "Orientation:" << wxyz[0] << " " << wxyz[1] << " "
    //   << wxyz[2] << " " << wxyz[3] << endl;
    // actor->RotateWXYZ(wxyz[0], wxyz[1], wxyz[2], wxyz[3]);
    cout << "Orientation:" << orientation[0] << " " << orientation[1] << " "
      << orientation[2] << endl;
    actor->RotateZ(orientation[2]);
    actor->RotateX(-orientation[0]);
    actor->RotateY(orientation[1]);
}

/**
 * @function main
 */
int main(int argc, char ** argv)
{
  // Prepare VTK
  //
  vtkSmartPointer<vtkPolyDataReader> pdReader =
    vtkSmartPointer<vtkPolyDataReader>::New();
  pdReader->SetFileName(const_cast<const char*>(argv[1]));
  pdReader->Update();

  // Create a mapper and actor
  vtkSmartPointer<vtkPolyDataMapper> mapper = 
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(pdReader->GetOutputPort());
  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);

  // Create a renderer, render window, and interactor
  vtkSmartPointer<vtkRenderer> renderer = 
    vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = 
    vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = 
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  // Add the actor to the scene
  renderer->AddActor(actor);
  renderer->SetBackground(1,1,1); // Background color white

  // Render and interact
  renderWindow->Render();

  // Initialize must be called prior to creating timer events.
  // renderWindowInteractor->Initialize();
  // Reading the input xml files name
  //char* str1 = argv[1];
  //char* str2 = argv[2];
  string face_cascade_name = argv[2];
  //string eyes_cascade_name = argv[2];
  //-- 1. Load the cascades
  //  if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };

  //-- 2. Read the video stream and depth data
  RGBD_IO v;
  boost::function<void ()> workerfun = v.run();
  boost::thread workerthread(workerfun);
  while (!v.is_ready()) {
      sleep(1);
  }

  Mat vis;
  for (int i = 0; i < 1000; i++) {
      cv::Mat frame = v.initImage();
      cv::Mat depth = v.initDepth();
      v.getImageAndDepth(&frame, &depth);
      cv::cvtColor(frame,frame,CV_RGB2BGR);
      // Store Faces 
      std::vector<Rect> faces_req;
      // Store ROI of faces
      //std::vector<Mat> faces_roi;
      //-- 3. Apply the classifier to the frame
      if( !frame.empty() ) {
          int maxind = detectAndTrack( face_cascade_name, &frame, &faces_req);
          if (maxind < 0)
            continue;
          frame.copyTo(vis);
          Rect face_rect = faces_req[maxind];
          Eigen::Matrix4f T = extractFeatures(frame, face_rect, depth, vis);


          rotateAndTranslate(actor, T);
          cout << "Transformation:" << T << endl;
          renderWindow->Render();
          imshow("c", vis );

          waitKey(33);

          // cout << "Points3d:"<< endl;
          // for (int i = 0;i < pts3d.size(); i ++) {
          //     cout << pts3d[i][0] << " " << 
          //       pts3d[i][1] << " " << 
          //       pts3d[i][2] << " " << endl;
          // }
      }
      else {
          printf(" --(!) No captured frame -- Break!");
          break;
      }
      imshow("c", vis );
      int c = waitKey(10);
      if( (char)c == 'c' ) break;

  }
  v.shutdown();
  workerthread.join();
  return 0;
}
